/*
 * Name: Fernando Do Nascimento
 * ID #: 1000638103
 * Programming Assignment 2
 * Description: This file contains all the libraries, constant variables and function
 * prototypes used in this lab assignment.
 */

/*
 * Libraries imported
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

/*
 * Defined constant
 */
#define MAX 256

/*
 * Function prototypes
 */
void catch_signal(int signum);
void shell_commands(char *command, char *reqParam, char *reqParam2, char *opParam, char *manParam,
	char *path);
