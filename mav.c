/*
 * Name: Fernando Do Nascimento
 * ID #: 1000638103
 * Programming Assignment 2
 * Description: This file has the source code of programming assigment 2.
 * The program performs a shell and based on the input entered by the user
 * , the program will execute an action. If the input is invalid, the
 * program will inform the user whether the command is invalid or the 
 * parameter of the command is invalid. If the input is valid, the program
 * will display the appropiate output. The program will exit if and only
 * if the user enters exit or quit.
 */

#include "libraries.h"

int main(void)
{
	int done = 0;
	char *delims = " ";

	/* 
	 * Signal Handlers used in order to prevent the program to 
	 * terminate when the user enters ctrl-c or ctrl-z.
	 */
	sig_t return_value = signal(SIGINT, catch_signal);
	
	signal(SIGTSTP, catch_signal);

	assert(return_value != SIG_ERR);

	while(!done)
	{
		char input[MAX], copy[MAX], reqParam[MAX], reqParam2[MAX];
		char opParam[MAX] = "-";
		char path[MAX] = "/bin/";
		char manParam[2];

		printf("msh> ");
		fflush(NULL);

		fgets(input, MAX, stdin);
		
		/*
  		 * Copies the input to the string copy because when performing a strtok the rest of 
		 * the input in the input string will be lost.
		 */
		strcpy(copy, input);		

		char *command = strtok(input, delims);
			
		/*
 		 * If the command entered by the user does not have any parameters, then the statement 
		 * below will remove the new line character from the string in order to concatenate the 
		 * command with the path and perform string comparisons. 
 		 */
		if(command[strlen(command) - 1] == '\n')
			command[strlen(command) - 1] = '\0';
		
				
		strcat(path, command);

		/*
   		 * If the command entered has the possibility of accepting optional parameters, then the 
		 * following statement will be executed. Where it will tokenize the copy string and 
		 * perform a string concatenate to the string storing all the parameters. Once it finished 
		 * going thru the entire string, the new line character will be deleted from the opParam 
		 * string in order to perform correctly execl.
		 */
		if(strcmp(command, "ls") == 0 || strcmp(command, "ps") == 0)
		{
			char *optional = strtok(copy, "-");

			while(optional != NULL)
			{
				optional = strtok(NULL, "- ");
				
				if(optional != NULL)
					strcat(opParam, optional);
					
			}
			
			opParam[strlen(opParam) - 1] = '\0';
		}

		/* 
   		 * If the command entered are cat, cd, mkdir, rm, rmdir, touch, or man. Then the following 
		 * statement will check if the input has the optional parameters for the manual input. If
		 * the parameters are found, they will be saved to string manParam in order to be used 
		 * when executing the command. Another operation that the statement performs is get and
		 * store the required parameter for each command. The last operation performed is removing
		 * the new line character from the required parameter string in order to successfully 
		 * execute the command.
		 */	
		if(strcmp(command, "cat") == 0 || strcmp(command, "cd") == 0 || strcmp(command, "mkdir") == 0 
			|| strcmp(command, "rm") == 0 || strcmp(command, "rmdir") == 0 || strcmp(command, "touch") 
				== 0 || strcmp(command, "man") == 0)
		{
			char *required = strtok(copy, delims);
			
			while(required != NULL)
			{
				if(strcmp(required, "1") == 0 || strcmp(required, "2") == 0 || strcmp(required, "3") == 0)
					strcpy(manParam, required);

				if(required != NULL)
					strcpy(reqParam, required);
			
				required = strtok(NULL, delims);
			}

			reqParam[strlen(reqParam) - 1] = '\0';
		}
 
		/*
		 * If the command entered has 2 required parameters, then the program will perform the following 
		 * operations. First it will tokenize the string in order to get the first parameter and store it
		 * in the string reqParam, then it will tokenize the string again to get the second parameter and
		 * store it in reqParam2. 
		 */
		int ctr = 0;
		if(strcmp(command, "cp") == 0)
		{
			char *required = strtok(copy, delims);

			while(required != NULL)
			{
				required = strtok(NULL, delims);
			
				if(ctr == 0 && required != NULL)
					strcpy(reqParam, required);

				if(ctr == 1 && required != NULL)
				{
					strcpy(reqParam2, required);

					reqParam2[strlen(reqParam2) - 1] = '\0';
				}
				
				ctr++;
			}
		}
	
		/*
		 * If the command entered was chmod, then the statement below will tokenize the input string
		 * and store the parameters in the appropiate string. 
		 * reqParameter - stores the mode
		 * reqParameter2 - stores the name of the file/directory
		 * opParam - stores the optional parameter -v if entered by the user
		 * i - integer loop counter
		 * param - char that determines if the optional parameter has been entered in the input.
		 */	
		int i = 0;		// loop counter
		char param = 'n';	// variable used to determine if optional parameter was entered.
		if(strcmp(command, "chmod") == 0)
		{
			char *parameter = strtok(copy, delims);

			while(parameter != NULL)
			{
				if(strcmp(parameter, "-v") == 0)
				{
					strcpy(opParam, parameter);
					
					param = 'y';
				}
				
				if(param == 'y')
				{
					if(i == 2)
						strcpy(reqParam, parameter);

					if(i == 3)
						strcpy(reqParam2, parameter);
				}

				if(param == 'n')
				{
					if(i == 1)
					{
						strcpy(reqParam, parameter);
						printf("%s\n", reqParam);
					}

					if(i == 2)
					{	
						strcpy(reqParam2, parameter);
						printf("%s\n", reqParam2);
					}
				}

				i++;

				parameter = strtok(NULL, delims);

			}
		
			reqParam2[strlen(reqParam2) - 1] = '\0';
		}
		/*
		 * Calls the function shell_commands in order to execute the command entered
		 * by the user. Based on the input, the function will display the correct output
		 * or a message stating that the input is invalid.
		 */
		shell_commands(command, reqParam, reqParam2, opParam, manParam, path);
	}

	return 0;
}

/*
 * Function: shell_commands
 * Parameter(s): command - pointer to the first element in the string command. Used to determine
 * the command entered by the user and the command for the program to execute.
 * reqParam - pointer to the first element of the string reqParam. Used to execute the required 
 * parameter of the command entered by the user.
 * reqParam - pointer to the first element of the string reqParam2. Used to execute the required
 * second parameter of the command entered by the user.
 * opParam - pointer to the first element of the string opParam. Used to execute the optional
 * parameter of the command that was entered by the user.
 * manParam - pointer to the first element of the string manParam. Used to execute the optional
 * parameter of the manual command. Manual Parameters = 1, 2, or 3.
 * path - pointer to the first element of the string path. Used to store the path of each command.
 * It is needed in order to execute each command succesfully.
 * Returns: void.
 * Description: The function creates a child and parent process. The child process executes every
 * command except 2. The exit/quit command and the change directory command.
 * Based on the input entered by the user the function will compare the input with a number of
 * if statements and perform the appropiate execution.
 * If the input entered is valid, the program will display to the user the output based on the 
 * commands entered.
 * If the user enters ctrl-c or ctrl-z the program will ignore the commands.
 * If the input is entered is invalid, the program will display a message.
 * If the user enters a valid command, but an invalid parameter, the program will inform the user
 * that the parameter was incorrect.
 */
void shell_commands(char *command, char *reqParam, char *reqParam2, char *opParam, char *manParam,
	char *path)
{
	pid_t pid = fork();

	if(pid == -1)
	{
		perror("fork failed");
		exit(EXIT_FAILURE);
	}	

	/*
  	 * Based on the commands entered by the user, the child process will execute the appropriate
	 * command and display the correct output. 
	 */
	else if(pid == 0)
	{
		/*
 		 * Calculates the size of the optional parameter array in order to determine if
		 * the command has an optional parameter or not. If the input is chmod and there
 		 * isn't an optional parameter the size of opSize = 1. If the input is ps or ls
 		 * and has no optional parameter the value of opSize = 0. 
		 */
		int opSize = strlen(opParam);

		if(strcmp(command, "\n") == 0 || strcmp(command, "exit") == 0 || strcmp(command, "quit") == 0
			|| strcmp(command, "cd") == 0)
			;

		else if(strcmp(command, "man") == 0)
		{
			int size = strlen(manParam);
			
			if(size == 0)
				execl("/usr/bin/man", command, reqParam, NULL);
		
			else
				execl("/usr/bin/man", command, manParam, reqParam, NULL);
		}

		else if(strcmp(command, "chmod") == 0)
		{
			if(opSize == 1)
				execl(path, command, reqParam, reqParam2, NULL);

			else
				execl(path, command, opParam, reqParam, reqParam2, NULL);
		}

		else if(strcmp(command, "cp") == 0)
			execl(path, command, reqParam, reqParam2, NULL);

		else if(strcmp(command, "env") == 0)
			execl(path, command, NULL);
		
		else if(strcmp(command, "ps") == 0)
		{
			if(opSize == 0)
				execl(path, command, NULL);

			else
				execl(path, command, opParam, NULL);
		}

		else if(strcmp(command, "pwd") == 0)
			execl(path, command, NULL);
			
		else if(strcmp(command, "rm") == 0 || strcmp(command, "rmdir") == 0 || 
			strcmp(command, "cat") == 0 || strcmp(command, "touch") == 0 ||
				strcmp(command, "mkdir") == 0)
			execl(path, command, reqParam, NULL);

	
		else if(strcmp(command, "ls") == 0)
		{
			if(opSize == 0)
				execl(path, command, NULL);

			else
				execl(path, command, opParam, NULL);
		}

		else
			printf("%s: Command not found.\n", command);

		exit(EXIT_SUCCESS);
	}

	/*
  	 * The parent process waits for the pid of the child to finish the process
	 * that is running. If the user enters exit or quit the parent process will
	 * termiante the program. Also if the user enters the command cd, the parent
	 * process will change the directory.	
	 */
	else
	{
		int status;
		(void) waitpid(pid, &status, 0);

		if(strcmp(command, "exit") == 0 || strcmp(command, "quit") == 0)
			exit(0);
		
		if(strcmp(command, "cd") == 0)
			chdir(reqParam);
	}
}

/*
 * Function: catch_signal
 * Parameter(s): signum - An integer value that represents the signal
 * number entered by the user (ctrl-c or ctrl-z).
 * Returns: void
 * Description: The function catches the signal entered by the user
 * and prevents the program from terminating by ignoring the signal.
 */
void catch_signal(int signum)
{
	if(signum == SIGINT || signum == SIGTSTP)
		fflush(stdout);
}
